//Ellis Rugazoora
//CSE 002 WelcomeClass
//Monday September 3rd 2018
public class WelcomeClass{
  
  public static void main(String args[]){
    System.out.println("   ----------- ");
    System.out.println("   | WELCOME |");
    System.out.println("   ----------- ");
    System.out.println("  ^  ^  ^  ^  ^  ^");
    System.out.println(" / \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-E--L--R--3--2--1->");
    System.out.println(" \\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("  v  v  v  v  v  v");
  }
}