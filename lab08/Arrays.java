//Ellis Rugazoora
//November 8 2018
//CSE 002, LAB 8 ARRAYS

public class Arrays{
  public static void main(String[] args){
    //My arrays are set up.
    int [] a = new int[100];
    int [] b = new int[100];
    
    int match = 0;
    System.out.print("Array one holds the integers: "); //Label for the line.
    for (int i = 0; i <= 99; i++){
    a[i] = (int) (Math.random()*99) + 0; //a random integer is printed between 0 and 99.
    System.out.print(a[i] + ", ");//the number in the array corresponding to i is printed.
    }
    
    System.out.println();//A new line is created before printing the occurences.
    
    for (int c = 0; c <= 99; c++){//every number in array a is checked against every number from 0 to 99.
      for(int i = 0; i <= 99; i++){//the first loop checks the first array int in a against 0.
        if (a[i] == c){ // the number of matches are tallied.
        match ++;
        }
        else{ //if there are no matches then that particular value in b corresponding to c is zero.
        b[c] = 0;
        }
      }
      b[c] = match; //match count is set back to zero.
      match = 0;
    }

      for (int c = 0; c <= 99; c++){
        if (b[c] != 0){//only numbers with atleast one match are printed.
        System.out.println(c + " occured " + b[c] + " times.");
        }
      }
   
    
  }
}