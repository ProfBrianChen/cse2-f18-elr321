//Ellis Rugazoora
//CSE 002 Craps switch
//24th 2018

import java.util.Scanner; //Scanner class is imported

public class CrapsSwitch{ //class is created
  public static void main(String[] args){ //main method is created
    
    Scanner myScanner = new Scanner(System.in); //System input is declared
    System.out.println("Would you like to randomly cast dice? Enter 1 for random or 2 for not"); //user is prompted
    int userAnswer = myScanner.nextInt();
    
    //dice values are declared
    int dice_One;
    int dice_Two;
    //random values are found regardless of users input
    int diceOne = (int)(Math.random()*6) + 1;
    int diceTwo = (int)(Math.random()*6) + 1;
    String rollname = ""; //empty string is created for rollname
    
switch(userAnswer){
      //in the event the user selected the random option  
 case 1: 
   
     System.out.println("Random dice values are " + diceOne + " and " + diceTwo + "."); //notification of random values 
        ///
    switch (diceOne){
       //the following switch is a check for dice two values given that dice one value is 1
         case 1:      
           switch(diceTwo){
           case 1: rollname = "Snake Eyes";
             System.out.println("You rolled a " + rollname);
           break;
           case 2: rollname = "Ace Deuce";
             System.out.println("You rolled a " + rollname);
           break;
           case 3: rollname = "Easy four";
             System.out.println("You rolled a " + rollname);
           break;
           case 4: rollname = "Fever five";
             System.out.println("You rolled a " + rollname);
           break;
           case 5: rollname = "Easy six";
             System.out.println("You rolled a " + rollname);
           break;
           case 6: rollname = "Seven out";
             System.out.println("You rolled a " + rollname);
           break;
         }
         break;
       case 2:
           //the following switch is a check for dice two values given that dice one value is 2
         switch(diceTwo){
           case 1: rollname = "Ace Deuce";
             System.out.println("You rolled a " + rollname);
           break;
           case 2: rollname = "Hard four";
             System.out.println("You rolled a " + rollname);
           break;
           case 3: rollname = "fever five";
             System.out.println("You rolled a " + rollname);
           break;
           case 4: rollname = "easy six";
             System.out.println("You rolled a " + rollname);
           break;
           case 5: rollname = "seven out";
             System.out.println("You rolled a " + rollname);
           break;
           case 6: rollname = "easy eight";
             System.out.println("You rolled a " + rollname);
           break;
         }
         break;
        case 3:
               //the following switch is a check for dice two values given that dice one value is 3
         switch(diceTwo){
           case 1: rollname = "Easy four";
             System.out.println("You rolled a " + rollname);
           break;
           case 2: rollname = "Fever five";
             System.out.println("You rolled a " + rollname);
           break;
           case 3: rollname = "hard six";
             System.out.println("You rolled a " + rollname);
           break;
           case 4: rollname = "seven out";
             System.out.println("You rolled a " + rollname);
           break;
           case 5: rollname = "easy eight";
             System.out.println("You rolled a " + rollname);
           break;
           case 6: rollname = "Nine";
             System.out.println("You rolled a " + rollname);
           break;
         }
         break;
       case 4:       //the following switch is a check for dice two values given that dice one value is 4
         switch(diceTwo){
           case 1: rollname = "Fever Five";
             System.out.println("You rolled a " + rollname);
           break;
           case 2: rollname = "Easy Six";
             System.out.println("You rolled a " + rollname);
           break;
           case 3: rollname = "Seven Out";
             System.out.println("You rolled a " + rollname);
           break;
           case 4: rollname = "hard eight";
             System.out.println("You rolled a " + rollname);
           break;
           case 5: rollname = "nine";
             System.out.println("You rolled a " + rollname);
           break;
           case 6: rollname = "easy ten";
             System.out.println("You rolled a " + rollname);
           break;
         }
         break;
       case 5:       //the following switch is a check for dice two values given that dice one value is 5
         switch(diceTwo){
           case 1: rollname = "Easy Six";
             System.out.println("You rolled a " + rollname);
           break;
           case 2: rollname = "Seven Out";
             System.out.println("You rolled a " + rollname);
           break;
           case 3: rollname = "Easy Eight";
             System.out.println("You rolled a " + rollname);
           break;
           case 4: rollname = "Nine";
             System.out.println("You rolled a " + rollname);
           break;
           case 5: rollname = "hard ten";
             System.out.println("You rolled a " + rollname);
           break;
           case 6: rollname = "yo eleven";
             System.out.println("You rolled a " + rollname);
           break;
         }
         break;
       case 6:       //the following switch is a check for dice two values given that dice one value is 6
         switch(diceTwo){
           case 1: rollname = "Seven Out";
             System.out.println("You rolled a " + rollname);
           break;
           case 2: rollname = "Easy Eight";
             System.out.println("You rolled a " + rollname);
           break;
           case 3: rollname = "Nine";
             System.out.println("You rolled a " + rollname);
           break;
           case 4: rollname = "Easy Ten";
             System.out.println("You rolled a " + rollname);
           break;
           case 5: rollname = "Yo-eleven";
             System.out.println("You rolled a " + rollname);
           break;
           case 6: rollname = "box cars";
             System.out.println("You rolled a " + rollname);
           break;
         }
         break;
      }
    
   
      break;
      
 case 2: //manual input option. not random
      System.out.println("Enter value of dice 1");
      dice_One = myScanner.nextInt();
      System.out.println("Enter value of dice 2");
      dice_Two = myScanner.nextInt();
      
      if (dice_One < 1 || dice_One > 6 || dice_Two < 1 || dice_Two > 6)  { //checking range of inputted values. Error message will occur if violated
      System.out.println("Enter values 1 through 6!");
      }
      else  {
       
      switch (dice_One) {
       case 1:       //the following switch is a check for dice two values given that dice one value is 1
           switch (dice_Two)
            {
           case 1: rollname = "Snake Eyes";
             System.out.println("You rolled a " + rollname);
           break;
           case 2: rollname = "Ace Deuce";
             System.out.println("You rolled a " + rollname);
           break;
           case 3: rollname = "Easy four";
             System.out.println("You rolled a " + rollname);
           break;
           case 4: rollname = "Fever five";
             System.out.println("You rolled a " + rollname);
           break;
           case 5: rollname = "Easy six";
             System.out.println("You rolled a " + rollname);
           break;
           case 6: rollname = "Seven out";
             System.out.println("You rolled a " + rollname);
           break;
            }
          break;
            
       case 2:       //the following switch is a check for dice two values given that dice one value is 2
            switch (dice_Two)
            {
           case 1: rollname = "Ace Deuce";
             System.out.println("You rolled a " + rollname);
           break;
           case 2: rollname = "Hard four";
             System.out.println("You rolled a " + rollname);
           break;
           case 3: rollname = "fever five";
             System.out.println("You rolled a " + rollname);
           break;
           case 4: rollname = "easy six";
             System.out.println("You rolled a " + rollname);
           break;
           case 5: rollname = "seven out";
             System.out.println("You rolled a " + rollname);
           break;
           case 6: rollname = "easy eight";
             System.out.println("You rolled a " + rollname);
           break;   
            }
          break;
       case 3:       //the following switch is a check for dice two values given that dice one value is 3
            switch (dice_Two)
            {
           case 1: rollname = "Easy Four";
                System.out.println("You rolled a " + rollname);
           break;
           case 2: rollname = "Fever Five";
                System.out.println("You rolled a " + rollname);
           break;
           case 3: rollname = "hard six";
             System.out.println("You rolled a " + rollname);
           break;
           case 4: rollname = "seven out";
             System.out.println("You rolled a " + rollname);
           break;
           case 5: rollname = "easy eight";
             System.out.println("You rolled a " + rollname);
           break;
           case 6: rollname = "nine";
             System.out.println("You rolled a " + rollname);
           break;    
            }
          break;
       case 4:       //the following switch is a check for dice two values given that dice one value is 4
            switch(dice_Two)
            {
           case 1: rollname = "Fever Five";
                System.out.println("You rolled a " + rollname);
           break;
           case 2: rollname = "Easy Six";
                System.out.println("You rolled a " + rollname);
           break;
           case 3: rollname = "Seven Out";
                System.out.println("You rolled a " + rollname);
           break;
           case 4: rollname = "hard eight";
             System.out.println("You rolled a " + rollname);
           break;
           case 5: rollname = "nine";
             System.out.println("You rolled a " + rollname);
           break;
           case 6: rollname = "easy ten";
             System.out.println("You rolled a " + rollname);
           break;            
            }
          break;
       case 5:       //the following switch is a check for dice two values given that dice one value is 5
            switch(dice_Two)
            {
           case 1: rollname = "Easy Six";
             System.out.println("You rolled a " + rollname);
           break; 
           case 2: rollname = "Seven Out";
             System.out.println("You rolled a " + rollname);
           break;
           case 3: rollname = "Easy Eight";
             System.out.println("You rolled a " + rollname);
           break;
           case 4: rollname = "Nine";
             System.out.println("You rolled a " + rollname);
           break;
           case 5: rollname = "hard ten";
             System.out.println("You rolled a " + rollname);
           break;
           case 6: rollname = "yo eleven";
             System.out.println("You rolled a " + rollname);
           break; 
            }
          break;
       case 6:       //the following switch is a check for dice two values given that dice one value is 6
            switch(dice_Two)
            {
           case 1: rollname = "Seven Out";
                System.out.println("You rolled a " + rollname);
           break;
           case 2: rollname = "Easy Eight";
                System.out.println("You rolled a " + rollname);
           break;
           case 3: rollname = "Nine";
                System.out.println("You rolled a " + rollname);
           break;
           case 4: rollname = "Easy Ten";
                System.out.println("You rolled a " + rollname);
           break;
           case 5: rollname = "Yo-eleven";
                System.out.println("You rolled a " + rollname);
           break;
           case 6: rollname = "box cars";
             System.out.println("You rolled a " + rollname);
           break;  
            }
          break;
        }
      
      }
    break; //break from the main switch
  }
  }
}
      
      
      
      
      
      
            
    