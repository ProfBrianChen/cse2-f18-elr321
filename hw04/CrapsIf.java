//Ellis Rugazoora
//CSE 002 Craps if
//September 24th 2018

import java.util.Scanner; //scanner is imported for user input

public class CrapsIf{ //class is created
  public static void main(String[] args){ //main method is created
    
    Scanner myScanner = new Scanner( System.in ); //System input is declared
    System.out.println("Would you like to randomly cast dice? Enter 0 for random or 1 for no"); //user is prompted for random or manual input
    int RandomOrNot = myScanner.nextInt(); //user input is an integer
    //Randomizer function is used. upper bound of six and lower boung of 1 (both inclusive.
    int diceOne = (int)(Math.random()*6) + 1;
    int diceTwo = (int)(Math.random()*6) + 1;
    //If user entered zero then the following if statement would be entered
    if (RandomOrNot == 0){
      //below are the operations for random section
      System.out.println("Your random values for the dice are " + diceOne + " and " + diceTwo + ".");//print statement for random values
      //in following if statements dice two values are checked against dice one values
      System.out.print("You rolled a: "); //print statement is set regardless of rollname
      if (diceOne == 1 && diceTwo == 1){ //boolean within if statements to match dice values and print corresponding roll pair
        System.out.println("Snake eyes"); 
      }
      else if (diceOne == 1 && diceTwo == 2){
        System.out.println("Ace Deuce");
      }
      else if (diceOne == 1 &&  diceTwo == 3){
        System.out.println("Easy Four");
      }
      else if (diceOne == 1 && diceTwo == 4){
        System.out.println("Fever Five"); 
      }
      else if (diceOne == 1 && diceTwo == 5){
        System.out.println("Easy Six"); 
      }
      else if (diceOne == 1 && diceTwo == 6){
        System.out.println("Seven Out"); 
      }
      else if (diceOne == 2 && diceTwo == 2){ //AT this point. dice one value is 1
        System.out.println("Hard Four"); 
      }
      else if (diceOne == 2 && diceTwo == 3){
        System.out.println("Fever Five"); 
      }
      else if (diceOne == 2 && diceTwo == 4){
        System.out.println("Easy six"); 
      }
      else if (diceOne == 2 && diceTwo == 5){
        System.out.println("Seven Out"); 
      }
      else if (diceOne == 2 && diceTwo == 6){
        System.out.println("Easy eight"); 
      }
      else if (diceOne == 3 && diceTwo == 3){
        System.out.println("hard six"); 
      }
      else if (diceOne == 3 && diceTwo == 4){
        System.out.println("seven out"); 
      }
      else if (diceOne == 3 && diceTwo == 5){
        System.out.println("easy eight"); 
      }
      else if (diceOne == 3 && diceTwo == 6){
        System.out.println("nine"); 
      }
      else if (diceOne == 4 && diceTwo == 4){
        System.out.println("hard eight"); 
      }
      else if (diceOne == 4 && diceTwo == 5){
        System.out.println("nine"); 
      }
      else if (diceOne == 4 && diceTwo == 6){
        System.out.println("easy ten"); 
      }
      else if (diceOne == 5 && diceTwo == 5){
        System.out.println("hard ten"); 
      }
      else if (diceOne == 5 && diceTwo == 6){
        System.out.println("yo eleven"); 
      }
      else if (diceOne == 6 && diceTwo == 6){
        System.out.println("boxcars"); 
      }
      
      //in following set of if statements dice one values are checked against dice two values
      
      else if (diceTwo == 1 && diceOne == 1){
        System.out.println("Snake eyes"); 
      }
      else if (diceTwo == 1 && diceOne == 2){
        System.out.println("Ace Deuce");
      }
      else if (diceTwo == 1 && diceOne == 3){
        System.out.println("Easy Four");
      }
      else if (diceTwo == 1 && diceOne == 4){
        System.out.println("Fever Five"); 
      }
      else if (diceTwo == 1 && diceOne == 5){
        System.out.println("Easy Six"); 
      }
      else if (diceTwo == 1 && diceOne == 6){
        System.out.println("Seven Out"); 
      }
      else if (diceTwo == 2 && diceOne == 2){
        System.out.println("Hard Four"); 
      }
      else if (diceTwo == 2 && diceOne == 3){
        System.out.println("Fever Five"); 
      }
      else if (diceTwo == 2 && diceOne == 4){
        System.out.println("Easy six"); 
      }
      else if (diceTwo == 2 && diceOne == 5){
        System.out.println("Seven Out"); 
      }
      else if (diceTwo == 2 && diceOne == 6){
        System.out.println("Easy eight"); 
      }
      else if (diceTwo == 3 && diceOne == 3){
        System.out.println("hard six"); 
      }
      else if (diceTwo == 3 && diceOne == 4){
        System.out.println("seven out"); 
      }
      else if (diceTwo == 3 && diceOne == 5){
        System.out.println("easy eight"); 
      }
      else if (diceTwo == 3 && diceOne == 6){
        System.out.println("nine"); 
      }
      else if (diceTwo == 4 && diceOne == 4){
        System.out.println("hard eight"); 
      }
      else if (diceTwo == 4 && diceOne == 5){
        System.out.println("nine"); 
      }
      else if (diceTwo == 4 && diceOne == 6){
        System.out.println("easy ten"); 
      }
      else if (diceTwo == 5 && diceOne == 5){
        System.out.println("hard ten"); 
      }
      else if (diceTwo == 5 && diceOne == 6){
        System.out.println("yo eleven"); 
      }
      else if (diceTwo == 6 && diceOne == 6){
        System.out.println("boxcars"); 
      }
    }
    else if (RandomOrNot == 1){
         //below are the operations for manual input
         System.out.println("Enter value of dice one");//prompt user for dice one value
         int dice_One = myScanner.nextInt(); //an integer
         System.out.println("Enter value of dice two");//prompt user for dice two value
         int dice_Two = myScanner.nextInt(); //an integer
         if (dice_One < 1 || dice_One > 6 || dice_One < 1 || dice_One > 6)  { //checking range of inputted values. Error message will occur if violated
           System.out.println("Enter values between 1 and 6!"); // error message if input is out of bounds
         }
       else{
         System.out.print("You rolled a: ");
         
       if (dice_One == 1 && dice_Two == 1){
        System.out.println("Snake eyes"); //print statement for what user rolled
      }
      else if (dice_One == 1 && dice_Two == 2){
        System.out.println("Ace Deuce");
      }
      else if (dice_One == 1 &&  dice_Two == 3){
        System.out.println("Easy Four");
      }
      else if (dice_One == 1 && dice_Two == 4){
        System.out.println("Fever Five"); 
      }
      else if (dice_One == 1 && dice_Two == 5){
        System.out.println("Easy Six"); 
      }
      else if (dice_One == 1 && dice_Two == 6){
        System.out.println("Seven Out"); 
      }
      else if (dice_One == 2 && dice_Two == 2){
        System.out.println("Hard Four"); 
      }
      else if (dice_One == 2 && dice_Two == 3){
        System.out.println("Fever Five"); 
      }
      else if (dice_One == 2 && dice_Two == 4){
        System.out.println("Easy six"); 
      }
      else if (dice_One == 2 && dice_Two == 5){
        System.out.println("Seven Out"); 
      }
      else if (dice_One == 2 && dice_Two == 6){
        System.out.println("Easy eight"); 
      }
      else if (dice_One == 3 && dice_Two == 3){
        System.out.println("hard six"); 
      }
      else if (dice_One == 3 && dice_Two == 4){
        System.out.println("seven out"); 
      }
      else if (dice_One == 3 && dice_Two == 5){
        System.out.println("easy eight"); 
      }
      else if (dice_One == 3 && diceTwo == 6){
        System.out.println("nine"); 
      }
      else if (dice_One == 4 && dice_Two == 4){
        System.out.println("hard eight"); 
      }
      else if (dice_One == 4 && dice_Two == 5){
        System.out.println("nine"); 
      }
      else if (dice_One == 4 && dice_Two == 6){
        System.out.println("easy ten"); 
      }
      else if (dice_One == 5 && dice_Two == 5){
        System.out.println("hard ten"); 
      }
      else if (dice_One == 5 && dice_Two == 6){
        System.out.println("yo eleven"); 
      }
      else if (dice_One == 6 && dice_Two == 6){
        System.out.println("boxcars"); 
      }
       
      else if (dice_Two == 1 && dice_One == 1){
        System.out.println("Snake eyes"); 
      }
      else if (dice_Two == 1 && dice_One == 2){
        System.out.println("Ace Deuce");
      }
      else if (dice_Two == 1 && dice_One == 3){
        System.out.println("Easy Four");
      }
      else if (dice_Two == 1 && dice_One == 4){
        System.out.println("Fever Five"); 
      }
      else if (dice_Two == 1 && dice_One == 5){
        System.out.println("Easy Six"); 
      }
      else if (dice_Two == 1 && dice_One == 6){
        System.out.println("Seven Out"); 
      }
      else if (dice_Two == 2 && dice_One == 2){
        System.out.println("Hard Four"); 
      }
      else if (dice_Two == 2 && dice_One == 3){
        System.out.println("Fever Five"); 
      }
      else if (dice_Two == 2 && dice_One == 4){
        System.out.println("Easy six"); 
      }
      else if (dice_Two == 2 && dice_One == 5){
        System.out.println("Seven Out"); 
      }
      else if (dice_Two == 2 && dice_One == 6){
        System.out.println("Easy eight"); 
      }
      else if (dice_Two == 3 && dice_One == 3){
        System.out.println("hard six"); 
      }
      else if (dice_Two == 3 && dice_One == 4){
        System.out.println("seven out"); 
      }
      else if (dice_Two == 3 && dice_One == 5){
        System.out.println("easy eight"); 
      }
      else if (dice_Two == 3 && dice_One == 6){
        System.out.println("nine"); 
      }
      else if (dice_Two == 4 && dice_One == 4){
        System.out.println("hard eight"); 
      }
      else if (dice_Two == 4 && dice_One == 5){
        System.out.println("nine"); 
      }
      else if (dice_Two == 4 && dice_One == 6){
        System.out.println("easy ten"); 
      }
      else if (dice_Two == 5 && dice_One == 5){
        System.out.println("hard ten"); 
      }
      else if (dice_Two == 5 && dice_One == 6){
        System.out.println("yo eleven"); 
      }
      else if (dice_Two == 6 && dice_One == 6){
        System.out.println("boxcars"); 
      }
    }
  }
}
}