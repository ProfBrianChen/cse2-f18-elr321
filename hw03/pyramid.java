//Ellis Rugazoora
//CSE 002 Pyramid
//Sunday September 16 2018

import java.util.Scanner; //Scanner class is imported

public class pyramid{
  public static void main(String[] args){
  Scanner myScanner = new Scanner( System.in ); //System input is declared
  System.out.print("Enter the square length of the pyramid: "); //The user is prompted for the square legnth of pyramid base
  double squareLength = myScanner.nextDouble(); //The square length is declared as double.
  System.out.print("Enter the height of the pyramid: "); //THe user is prompted for the vertical height of the pyramid.
  double pyramidHeight = myScanner.nextDouble(); //Height of pyramid may not be a whole number. declared as double.
  double pyramidVolume = (Math.pow(squareLength, 2)) * pyramidHeight / 3; //operation for volume of a square based pyramid= (a^2)(h)/3
  System.out.println("The volume of the pyramid is " + pyramidVolume + " cubic units."); //print statement for volume of pyramid
    
  }
}