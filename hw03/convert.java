//Ellis Rugazoora
//CSE 002 Convert
//Sunday September 16 2018

import java.util.Scanner; //Scanner class is imported

public class convert{
  public static void main(String[] args){
    Scanner myScanner = new Scanner( System.in ); //System input is declared
    System.out.print("Enter the area affected by the rain in acres: "); //The user is prompted for the area affected
    double areaAffected = myScanner.nextDouble(); //The area is declared as a double.
    System.out.print("Enter the rainfall in these areas (inches): "); //THe user is prompted for amount of rain (inches)
    double rainFall = myScanner.nextDouble(); //rainfall is declared as double.
    
    double volumeAcreInches = areaAffected * rainFall;//operation for volume in acre-inches
    double GallonsPerAcreInch = 27154.29; //gallon : acreinch conversion ratio
    double cubicMilesPerGallon = 1 / 1101117147428.6;// cubicmile : gallon conversion ratio
    double volumeCubicMiles = (int)(volumeAcreInches * GallonsPerAcreInch * cubicMilesPerGallon * 100000000) / 100000000.0;//operation to reduce to 8 demical places
    
    System.out.println("The volume of water is " + volumeCubicMiles + " cubic miles."); //print statement for volume in cubic miles
    
  }
}