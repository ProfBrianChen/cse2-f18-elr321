//Ellis Rugazoora
//CSE 002 LABO2 CYCLOMETER
//September 6 2018

public class Cyclometer {
  public static void main (String[] args){
  
    //our input data
    //Declared as integers
    int secsTrip1 = 480;
    int secsTrip2 = 3220;
    int countsTrip1 = 1561;
    int countsTrip2 = 9037;
    
    //our intermediate variables and output data
    double wheelDiameter = 27.0,
    PI = 3.14159, //pi will be used to calculate circumference of wheel
    feetPerMile = 5280,     //ratio of feet to miles
    inchesPerFoot = 12,    //ratio of inches to feet
    secondsPerMinute = 60;     //ratio of seconds to minutes
    double distanceTrip1, distanceTrip2, totalDistance;     //Variables are declared as doubles
    //operations for trip time were made inside the print statement. seconds converted to minutes using conversion ratio
    System.out.println("Trip 1 took " + (secsTrip1/secondsPerMinute) + " minutes and had " + countsTrip1+ " counts.");
    System.out.println("Trip 2 took " + (secsTrip2/secondsPerMinute) + " minutes and had " + countsTrip2+ " counts.");
    
    //Wheel circumference in times number of revolutions amounts to distance travelled in inches. This is converted to miles.
    //Same operations for both trips
    distanceTrip1 = countsTrip1 * PI * wheelDiameter / feetPerMile / inchesPerFoot;
    distanceTrip2 = countsTrip2 * PI * wheelDiameter / feetPerMile / inchesPerFoot;
    totalDistance = distanceTrip2 + distanceTrip1;
    
    //print out the output data for trip 1, trip 2 and total distance
    System.out.println("Trip 1 was " + distanceTrip1 + " miles");
    System.out.println("Trip 2 was " + distanceTrip2 + " miles");
    System.out.println("The total distance was " + totalDistance + " miles");
    
  }
}
