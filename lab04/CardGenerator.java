//Ellis Rugazoora
//CSE 002 Card Generator
//Thursday September 20th 2018


public class CardGenerator{ //class is created
  public static void main(String[] args){ //main method is created
    
   int cardNumber = (int)(Math.random()*53) + 1; //cardnumber is a random integer that is bounded between 1 and 52 both inclusive
     
    String suit = ""; //empty string is created for the card suit
    String identity = ""; //empty string is created for the card identity
   
  if (cardNumber > 0  && cardNumber <= 13){ //if statement checks for whether card is within diamonds range. if so suit is assigned diamond
    suit = "diamonds";
  }
  else if (cardNumber > 13  && cardNumber <= 26){ //else if statement assigns card number between 13 and including 26 to clubs.
    suit = "clubs";
    cardNumber = cardNumber - 13; //lowerbound is subtracted from card number. new cardnumber can now go thru switch.
    }
  else if (cardNumber > 26  && cardNumber <= 39){ //else if statement assigns card number between 26 and including 39 to hearts
    suit = "hearts";
    cardNumber = cardNumber -26; //lowerbound is subtracted from card number so new cardnumber can now go thru switch.
    }
  else if (cardNumber > 39  && cardNumber <= 52){ //else if statement assigns card #'s within range the spades suit.
    suit = "Spades";
    cardNumber = cardNumber - 39;//lowerbound is subtracted from card number so new cardnumber can now go thru switch.
  }
       
    switch(cardNumber){ //after if statements, the cardnumber goes through switch in which there are 13 cases, one for each identity.
      case 1:
       identity = "Ace"; //identity string is assigned according value of cardnumber
       break; //break is used to leave the switch after a match
     case 2:
       identity = "2";
       break;
     case 3:
       identity = "3";
       break;
     case 4:
       identity = "4";
       break;
     case 5:
       identity = "5";
       break;
     case 6:
       identity = "6";
       break;
     case 7:
       identity = "7";
       break;
     case 8:
       identity = "8";
       break;
     case 9:
       identity = "9";
       break;
     case 10:
       identity = "10";
       break;
     case 11:
       identity = "Jack"; //the jack is the royal equivalent of 11
       break;
     case 12:
       identity = "Queen"; //the queen is the royal equivalent of 12
       break;
     case 13:
       identity = "King"; //the king is the royal equivalent of 13 in deck.
       break;
  }
    System.out.println("Your card is " + identity + " of " + suit); //print statement stating identity and suit of card.
}
}