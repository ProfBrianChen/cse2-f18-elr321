//Ellis Rugazoora
//CSE 002 Bill Check
//Tuesday September 13th 2018

//This program determines the different ways a dining bill can be split between friends based on input about the check and percentage tip.
import java.util.Scanner; //Scanner class is imported

public class Check{
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner( System.in ); //System input is declared 
    System.out.print("The original cost of the check is: $"); //The user is prompted for the cost of the check
    double checkCost = myScanner.nextDouble(); //The check cost is a double with two decimals places. it is inputted
    System.out.print("What percent would you like to tip (enter as percent)?: ");//The user is prompted for the percent tip
    double tipPercent = myScanner.nextDouble(); //It is entered as integer 
    double decimalTip = tipPercent / 100; //the integer is converted to a decimal
    System.out.print("Enter the number of people that went out to dinner: "); //THe user is prompted to number of people
    int numPeople = myScanner.nextInt(); //Number of people is obviously an integer
    
    double totalCost; //total cost is declared as double
    double costPerPerson; //cost per person is declared as double
    totalCost = checkCost * (1 + decimalTip); //check cost times tax decimal plus one equals total cost
    costPerPerson = totalCost / numPeople; //cost per person is total cost divided by number or people
    int dollars = (int)costPerPerson; //decimals are dropped from cost to get number of dollars
    int dimes = (int)(costPerPerson * 10) % 10; //cost per person time'sd by 10 and converted to integer. Remainder operation used to find final digits
    int cents = (int)(costPerPerson * 100) % 10; //remainder operation used to find cents
    System.out.print("Each person owes $" + dollars + "." + dimes + cents); //print statements dollars dimes and cents are added individually
    
    
  }
}