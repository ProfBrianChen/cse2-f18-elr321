//Ellis Rugazoora
//CSE 002 lab05
//4th October 2018

import java.util.Scanner; //Scanner class is imported

public class UserInput{
  public static void main(String[] args){
  Scanner myScanner = new Scanner( System.in ); //System input is declared
    
    int a; //course code is declared as integer
    String b = ""; //department name is a string
    int c; //number of meeting times is a integer
    int d; //meeting time is an integer
    String e = "";//instructer name is an integer
    int f;//number of students in course is an integer
    
    System.out.println("Please enter the course code for your course"); //User is prompted for course code.
    while (!myScanner.hasNextInt()){ //if input is not an integer then condition is true, and will loop until integer is inputted.
      myScanner.next(); //scans most recent input
      System.out.println("Error, please enter an integer"); //error message if not an integer.
    }
    a = myScanner.nextInt(); //first integer outside will be stored in "a".
    
    System.out.println("Please enter the department name for your course");//User is prompted for dept' name.
    
    b = myScanner.nextLine(); //scans most recent input string and will be stored in "b" string.
    
    System.out.println("Please enter the number of meeting times in a week for your course");//user is promted for meeting times per week
    while (!myScanner.hasNextInt()){//if input is not an integer then condition is true, and will loop until integer is inputted.
      myScanner.next();//scans most recent input
      System.out.println("Error, please enter an integer");//error message if not an integer.
    }
    //next integer will be stored in "c".
    c = myScanner.nextInt(); 
    
    System.out.println("Please enter the meeting time for your course in military time"); //user 
    while (!myScanner.hasNextInt()){//if input is not an integer then condition is true, and will loop until integer is inputted.
      myScanner.next();//scans most recent input
      System.out.println("Error, please enter an integer");//error message if not an integer.
    }
    //next integer will be stored in "d".
    d = myScanner.nextInt(); //next integer will be stored in "d".
    
    System.out.println("Please enter the professor name for your course"); //user is promped for instructor name
    e = myScanner.nextLine(); //scans next line and stores in string "e".
    
    System.out.println("Please enter the number of students in the course");// user is promped for number of students.
    while (!myScanner.hasNextInt()){//if input is not an integer then condition is true, and will loop until integer is inputted.
      myScanner.next(); //scans most recent input
      System.out.println("Error, please enter an integer");//error message if not an integer.
    }
    //next integer will be stored in string "f".
    f = myScanner.nextInt();
    
    
    
  ///// Below are the print statments for user inputted information about course.
    
    System.out.println("The course code for course one is " + a);
    System.out.println("The department name for the course is " + b);
    System.out.println("The number of meeting times in a week for the course is " + c);
    System.out.println("The meeting time for the course is " + d);
    System.out.println("The professor's name is " + e);
    System.out.println("The course code for course one is " + f);




  }
}