//Ellis Rugazoora
//CSE 002 HW06
//20TH OCTOBER 2018

import java.util.Scanner; //scanner class imported

public class EncryptedX{
  public static void main(String[] args){
    Scanner myScanner = new Scanner (System.in); //scanner set up
    
    int side, a, b, c; //four integers are declared
    
  System.out.println("Enter an integer between 1 and 100"); //prompt user for integer
  
  while (!myScanner.hasNextInt()){   //will execute loop if input is not an integer
    myScanner.next();
    System.out.println("Please Enter an integer"); //prompt to enter an integer.
    }
  side = myScanner.nextInt(); //next int is assigned to variable side.
  
  if (side > 100 || side < 0){ //if side is out of range, while loop below will execute.
    while (side > 100 || side < 0){
      System.out.println("Print an integer between 0 and 100"); //prompt to enter int within range.
      side = myScanner.nextInt();
    }
  }

for (a = 1; a <= side; a++){ //side determines the number of rows. New Row per interation of "a".
 
  for(b = 1; b <= side; b++){ //side determines the number of characters per row. New character per iteration of "b"
  c = (side - a) + 1; //variable c is the character number along the row in which a gap will be found.
  if (b == side){ //b == side represents the final character of a row. therefore println will be used in here.
   if (b == a || b == c){
   System.out.println(" "); //a space will be printed for either of these conditions
   }
   else{
   System.out.println("*"); //otherwise a star will be printed and a new line created as well.
   }
  }
  else if (b == a || b == c){ //spaces can be printed whether they are on the edge or not, only condition matters.
  System.out.print(" ");//print not println is used because they arent on the edge.
  }
  else{
  System.out.print("*"); //print not println is used because they arent on the edge.
  }
  
 }
    
}
    
  }
}