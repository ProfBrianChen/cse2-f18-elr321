//Ellis Rugazoora
//26/november/2018
//CSE 002
//HW09

import java.util.Scanner;
import java.util.Random;
public class CSE2Linear{
  public static void main (String[] arg){//this is the main method 
//prompt user for 15 integers
  Scanner myScanner = new Scanner(System.in);
  
  int[] grade = new int[15];
  
  for (int i = 0; i <= 14; i++){ //this for loop iterates FIFTEEN times
    System.out.println("Kindly Enter an integer between 0 and 100");
    while (!myScanner.hasNextInt()){   //will execute loop if input is not an integer
    myScanner.next();
    System.out.println("Please Enter an integer"); //prompt to enter an integer.
    }
  grade[i] = myScanner.nextInt(); //next int is assigned to variable side.
  
  if (grade[i] > 100 || grade[i] < 0){ //if side is out of range, while loop below will execute.
    while (grade[i] > 100 || grade[i] < 0){
      System.out.println("Enter an integer between 0 and 100"); //prompt to enter int within range.
      //After every prompt, the input type should be checked.
      grade[i] = myScanner.nextInt();
    }
  }
  
  if (i > 2){
    while (grade[i] < grade[i-1]){
        System.out.println("Enter an integer greater to or equal to the last."); //enter an integer greater or equal to than the last
        //After every prompt, the input type should be checked.
        grade[i] = myScanner.nextInt();
        }
  }
  }
  
  
  for (int t = 0; t < grade.length; t++){
    if (t == (grade.length - 1)){
      System.out.println(grade[t] + ".");
    }
    else{System.out.print(grade[t] + ", ");}
  }
  
  Scramble(grade); //scramble method is called for grade array.
  
  System.out.println("Please enter a grade you would like to search for:");
  int search = myScanner.nextInt();
  int indexOf = linearSearch(grade, search);
  
  if (indexOf == -1){
    System.out.println("The grade was not found");
  }
  else{
    System.out.println("The index of the grade is " + indexOf);
  }
  
  }
//THIS IS THE END OF MAIN METHOD.
  
public static void Scramble(int[] a){//this is the scramble method.
  Random ran= new Random ();
  for (int k = 1; k <= 20; k++){
  int d = ran.nextInt(14);
  int e = ran.nextInt(14);
  while (d == e){
  d = ran.nextInt(14);
  e = ran.nextInt(14);
  }
  int temp = a[d];
  a[d] = a[e];
  a[e] = temp;
  
  }
  for (int l = 0; l < a.length; l++){
    if (l == (a.length - 1)){
      System.out.println(a[l] + ".");
    }
    else{
      System.out.print(a[l] + ", ");
    }
  }
}
//end of scramble method

//linear search array
 public static int linearSearch(int[] list, int key) { //This is the linear search method.
  for (int i = 0; i < list.length; i++) 
  {
   if (key == list[i]) return i;
  }

 return -1;
 }
 
}