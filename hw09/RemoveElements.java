//Ellis Rugazoora
//November 27 2018
//cse 002 hw 09

import java.util.Scanner;

public class RemoveElements{

    public static int[] remove(int[] intArray, int intRemove) { ///BELOW IS THE REMOVE METHOD.
        int[] temp = intArray;
        int[] newArray = new int[10];
        int num = 0;
        int ArrayLength = 0;
        boolean whether = true;
        for(int i = 0; i < 10; i++) {
            for(int j = 0; j < 10; j++) {
                if(temp[j] == intRemove) {
                    newArray[i] = j;
                    temp[j] = -1;
                    break;
                }
            }
        }
        while(whether) {
            if(newArray[num] != 0) {
                ArrayLength++;
                num++;
            }
            else {
                whether = false;
            }
        }
        int[] result = new int[10 - ArrayLength];
        for(int i = 0; i < 10 - ArrayLength; i++) {
            for(int j = 0; j < 10; j++) {
                if(temp[j] > -1) {
                    result[i] = temp[j];
                    temp[j] = -1;
                    break;
                }
            }
        }
        return result;
    }
    /////END OF METHOD//////
    
    
        public static int[] delete(int[] intArray, int index) { ///BELOW IS THE DELETE METHOD
        int[] b = new int[intArray.length - 1];
        for(int i = 0; i < index;i++) {
            b[i] = intArray[i];
        }
        for(int i = index; i < 9;i++) {
            b[i] = intArray[i + 1];
        }
        return b;
    }
        
            //randomInput: fill a array which length is 9 with random number
    public static int[] randomInput() { ///BELOW IS THE RANDOM INPUT METHOD
        int[] a = new int[10];
        for(int i = 0; i < 10; i++) {
            a[i] = (int)(Math.random() * 10);
        }
        return a;
    }
        
        

    ///////BELOW IS THE MAIN METHOD ////////
    
    public static void main(String[] args){
        Scanner scan=new Scanner(System.in);
        int num[]=new int[10];
        int newArray1[];
        int newArray2[];
        int index,target;
        String answer="";
        do{
            System.out.print("Random input 10 ints [0-9]");
            num = randomInput();
            String out = "The original array is:";
            out += listArray(num);
            System.out.println(out);

            System.out.print("Enter the index ");
            index = scan.nextInt();
            newArray1 = delete(num,index);
            String out1="The output array is ";
            out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
            System.out.println(out1);

            System.out.print("Enter the target value ");
            target = scan.nextInt();
            newArray2 = remove(num,target);
            String out2="The output array is ";
            out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
            System.out.println(out2);

            System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
            answer=scan.next();
        }while(answer.equals("Y") || answer.equals("y"));
    }

    public static String listArray(int num[]){
        String out="{";
        for(int j=0;j<num.length;j++){
            if(j>0){
                out+=", ";
            }
            out+=num[j];
        }
        out+="} ";
        return out;
    }
}