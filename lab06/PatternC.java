//Ellis Rugazoora
//CSE 002 lab05
//11th October 2018

import java.util.Scanner; //Scanner class is imported


public class PatternC{
  public static void main(String[] args){
    //integers are declared
    int a, b, c, d, e;
    for (a = 6; a > 0 ; a--){ // a is initialized at 6 and decremented by 1
      e = a - 1; //"e" determines how many spaces there should be. For the first iteration there are 5 spaces.
        for (b = 1; b <= e; b++){ //Number of spaces is determined by "e". which decreases by each loop.
          System.out.print(" ");
        }
      d = 7 - a; //"d" determines how many integers are printed for each iteration of a.
        for(c = d; c > 0; c--){ //Integer "c" is initalized at "d" because printing is done is descending order.
          if (c == 1){ //1 is the final integer for each row, so a new line is created after it is printed.
            System.out.println(c);
          }
        else{//other wise a new line isnt created.
          System.out.print(c);
        }
      }
    }
  }
}