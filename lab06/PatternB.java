//Ellis Rugazoora
//CSE 002 lab05
//11th October 2018

import java.util.Scanner; //Scanner class is imported


public class PatternB{
  public static void main(String[] args){
    //integers are declared.
    int a;
    int b;
    for (a = 6; a > 0; a--){ //a is initialed at 6 and decremented by 1
      for (b = 1; b <= a; b++){ //b is initialized at 1 and is incremented by 1 
	    if (b == a){ //when b is equal to a then a new line is printed, otherwise not.
	      System.out.println(b);
	    }
	    else{
        System.out.print(b);
	    }
	
      }
    }
  }
}