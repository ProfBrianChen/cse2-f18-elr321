//Ellis Rugazoora
//CSE 002 lab05
//11th October 2018

import java.util.Scanner; //Scanner class is imported


public class PatternD{
  public static void main(String[] args){
    //integers are declared.
    int a;
    int b;
    for (a = 6; a > 0 ; a--){ //a is initialized at 6 and decremented by 1.
      for (b = a; b > 0; b--){ //for every value of "a", the initial value of "b" is equal to "a" and is decremented.
	    if ( b == 1){
	    System.out.println(b); //A new line is created after printing the last integer of the row.
	     }
      else{ //to avoid printing "b" twice.
       System.out.print(b); 
      }
	
      }
     }
    
   
  }
}