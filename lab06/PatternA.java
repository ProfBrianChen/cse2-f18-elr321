//Ellis Rugazoora
//CSE 002 lab05
//11th October 2018

import java.util.Scanner; //Scanner class is imported


public class PatternA{
  public static void main(String[] args){
    int a;
    int b;
    //above integers are declared
    for (a = 1; a <= 6; a++){//a is incremented by 1 until 6
      for (b = 1; b <= a; b++){ //for each value of a, every integer b is printed that is equal to or less than a.
	    if (b == a){ //when printing the last integer of the row (which is equal to a), a new line is created.
	      System.out.println(b);
        
	    }
      else{
              System.out.print(b); //this will be executed except for final integer of the row
      }
	
      }
    }
    
   
  }
}
