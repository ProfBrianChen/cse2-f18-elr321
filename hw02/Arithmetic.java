//Ellis Rugazoora
//CSE 002 Arithmetic
//Tuesday September 11th 2018

public class Arithmetic{
  public static void main (String[] args){
int numPants = 3; // number of pants declared as integer
int numShirts = 2; //number of shirts
int numBelts = 1; //number of belts

double pantsPrice = 34.98; //costs of pants, shirts, & belts declared as doubles
double shirtsPrice = 24.99; //cost of shirt
double beltsPrice = 33.99; //cost of belts
double paSalesTax = 0.06; // 6 percent tax on all purchases

double totalCostofPants, totalCostofShirts, totalCostofBelts, totalPretax; //totals declared as doubles
double taxPants, taxShirts, taxBelts, totalTax, amountDue; //tax totals declared as doubles

totalCostofPants = numPants * pantsPrice; //number of pants times cost per pant yields total cost of pants
totalCostofShirts = numShirts * shirtsPrice; //number of shirts times cost per shirt yeilds totalCostofShirts
totalCostofBelts = numBelts * beltsPrice; // #of belts x price per belt = total cost of belts 
totalPretax = totalCostofPants + totalCostofShirts + totalCostofBelts;//sum of total costs of each type yeild total cost before tax
    
taxPants = paSalesTax * totalCostofPants; //sales tax times total pants cost yield the tax on the pants
    double taxPantsprint = ((int)(taxPants * 100)) / 100.00; //tax converted to an integer AFTER multiplying by 100 so only 2 decimal places would remain when divided by 100.
taxShirts = paSalesTax * totalCostofShirts; //sales tax times total shirts cost yields tax on the shirts
    double taxshirtprint = ((int)(taxShirts * 100)) / 100.00; //same operation as in line 25 to give only 2 decimal places
taxBelts = paSalesTax * totalCostofBelts; //sales tax times total belts cost yields the tax on the belts
    double taxBeltprint = ((int)(taxBelts * 100)) / 100.00; //same operation as in line 25 to give only 2 decimal places
totalTax = taxPants + taxShirts + taxBelts; //sum of the tax on each type of clothing makes the total tax
    double totalTaxprint = ((int)(totalTax * 100)) / 100.00; //same operation as in line 25 to give only 2 decimal places
amountDue = totalPretax + totalTax;
    double amountDueprint = ((int)(amountDue * 100)) / 100.00; //same operation as in line 25
    
System.out.println("The pants total is $" + totalCostofPants + " and the tax on them is $" + taxPantsprint + "."); //print statement for pants' total cost and tax.
System.out.println("The shirts total is $" + totalCostofShirts + " and the tax on them is $" + taxshirtprint + ".");//print statement for shirts total cost and tax.
System.out.println("The belts total is $" + totalCostofBelts + " and the tax on them is $" + taxBeltprint + ".");//print statement for belts total cost and tax.
System.out.println("The total cost of the items before tax is $" + totalPretax + " and the total tax is $" + totalTaxprint + "."); //totals before tax and total tax.
System.out.println("The total amount due is $" + amountDueprint + "."); //print statement for total amount. total tax plus total cost before tax.
  }
}