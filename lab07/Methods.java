//Ellis Rugazoora
//CSE 002 lab
//20TH OCTOBER 2018

import java.util.Scanner; //scanner class imported

public class Methods{
    
    public static String randomAdjective(){ //this method return an adjective.
      String adjective = " ";//an empty string is declared
      int randomAdjectiveint = (int)(Math.random()*10) + 1; //random number generated between 1 and 10.
      switch(randomAdjectiveint){//the number is then run through this switch statement to find random word.
      case 1:
        adjective = "awesome";
        break;
      case 2:
        adjective = "stupendous";
        break;
      case 3:
        adjective = "radical";
        break;
      case 4:
        adjective = "Great";
        break;
      case 5:
        adjective = "ridiculous";
        break;
      case 6:
        adjective = "insane";
        break;
      case 7:
        adjective = "aggresive";
        break;
      case 8:
        adjective = "agreeable";
        break;
      case 9:
        adjective = "unique";
        break;
      case 10:
        adjective = "artistic";
        break;
      }
      return adjective;
    }
    
  public static String randomApe(){//this method returns ape names.
      String Ape = " ";//an empty string is declared
      int randomApeint = (int)(Math.random()*10) + 1;//random number generated between 1 and 10.
      switch (randomApeint){//the random integer is ran through the switch
        case 1:
          Ape = "Orangutan";//random ape names are picked
          break;
        case 2:
          Ape = "Chimpanzee";
          break;
        case 3:
          Ape = "Gorilla";
          break;
        case 4:
          Ape = "Siamang";
          break;
        case 5:
          Ape = "Bonobo";
          break;
        case 6:
          Ape = "Senge";
          break;
        case 7:
          Ape = "Mandrill";
          break;
        case 8:
          Ape = "Proboscis";
          break;
        case 9:
          Ape = "Capuchin";
          break;
        case 10:
          Ape = "Baboon";
          break;
      }
      return Ape;
    }
    
  public static String randomNoun(){//this method return a noun.
      String Noun = " ";//an empty string is declared
      int randomNounint = (int)(Math.random()*10) + 1;//random number generated between 1 and 10.
      switch (randomNounint){//the random integer is ran through the switch
        case 1:
          Noun = "building";//random nouns are picked.
          break;
        case 2:
          Noun = "computer";
          break;
        case 3:
          Noun = "dog";
          break;
        case 4:
          Noun = "tiger";
          break;
        case 5:
          Noun = "bathroom";
          break;
        case 6:
          Noun = "pencil";
          break;
        case 7:
          Noun = "whiteboard";
          break;
        case 8:
          Noun = "lawnmower";
          break;
        case 9:
          Noun = "juice";
          break;
        case 10:
          Noun = "hair";
          break;
      }
      return Noun;
    }
  public static String randomArticle(){//this method returns articles.
    String article = " ";//an empty string is declared
    int randomArticleint = (int)(Math.random()*10) + 1;//random number generated between 1 and 10.
    switch(randomArticleint){//the random integer is ran through the switch
        case 1:
          article = "her";
          break;
        case 2:
          article = "its";
          break;
        case 3:
          article = "their";
          break;
        case 4:
          article = "his";
          break;
        case 5:
          article = "our";
          break;
        case 6:
          article = "your";
          break;
        case 7:
          article = "my";
          break;
        case 8:
          article = "his";
          break;
        case 9:
          article = "her";
          break;
        case 10:
          article = "his";
          break;
  }
    return article;
  }
  public static String randomCapitalArticle(){//this method returns articles that are capitalized. for sentence opening.
    String article = " ";//an empty string is declared
    int randomArticleint = (int)(Math.random()*10) + 1;//random number generated between 1 and 10.
    switch(randomArticleint){//the random integer is ran through the switch
        case 1:
          article = "Her";
          break;
        case 2:
          article = "Its";
          break;
        case 3:
          article = "Their";
          break;
        case 4:
          article = "His";
          break;
        case 5:
          article = "Our";
          break;
        case 6:
          article = "That";
          break;
        case 7:
          article = "My";
          break;
        case 8:
          article = "His";
          break;
        case 9:
          article = "Her";
          break;
        case 10:
          article = "His";
          break;
  }
    return article;
  }
  
  public static void randomSentence(String a){//random sentence is constructed by calling other subsidiary methods.
   System.out.print("The " + randomAdjective() + " " + a + " " + randomVerb() + " " + randomArticle() + " " + randomNoun() + ".");
  }
  public static void randomSupport(String a){//random support sentence is constructed by calling other subsidiary methods.
    System.out.print(" " + a + " " + randomAdjective() + " " + randomApe() + " " + randomVerb() + " " + randomArticle() + " " + randomNoun() + ".");
  }
  public static void randomConclusion(String a){//random concluding sentence is constructed by calling other subsidiary methods.
    System.out.println(" " + randomCapitalArticle() + " " + a + " " + randomVerb() + " " + randomArticle() + " " +randomNoun() + ".");
  }
  public static void actionSentences(){ //this method calls all subsidiary methods
      String commonNoun = randomApe(); //Ensures that subject of conlusion and thesis are equal.
      String randomSupportSubject = randomIt(commonNoun + "'s");//randomly alternates between 
      randomSentence(commonNoun);
      randomSupport(randomSupportSubject);
      randomConclusion(commonNoun);
  }
  public static String randomVerb(){//this method returns a verb.
    String verb = " ";//an empty string is declared.
    int randomVerb = (int)(Math.random()*10) + 1;//random number generated between 1 and 10.
    switch(randomVerb){ //the random integer is ran through the switch
      case 1:
        verb = "walked";
        break;
      case 2:
        verb = "jumped";
        break;
      case 3:
        verb = "disturbed";
        break;
      case 4:
        verb = "irritated";
        break;
      case 5:
        verb = "worked";
        break;
      case 6:
        verb = "greeted";
        break;
      case 7:
        verb = "nagged";
        break;
      case 8:
        verb = "taught";
        break;
      case 9:
        verb = "paid";
        break;
      case 10:
        verb = "built";
        break;
    }
    return verb;
  }
  
  public static String randomIt(String a){
    int random = (int)(Math.random()*2) + 1; //randomly picked between its or the input (which is a noun).
    String randomItorSubject = " ";
    switch(random){
      case 1:
        randomItorSubject = "Its";
        break;
      case 2:
        randomItorSubject = "The " + a;
        break;
    }
    return randomItorSubject;
  }
    
  public static void main(String[] args){ //this is the main method
    
    Scanner myScanner = new Scanner(System.in);
    int yesOrNo = 1;//the while loop will run atleast once.
    while (yesOrNo == 1){ //While loop that continouosly prompts the use for input.
      actionSentences();//single method that calls all subsidiary methods.
      System.out.println("Do you want to print another fun sentence. Enter 1 for yes, and 2 for no");//user is prompted
      yesOrNo = myScanner.nextInt();//input is enabled.
  }
    
  }
  }
  
